function countLetter(letter, sentence) {
    let result = 0;

    if (typeof letter !== 'string' || letter.length !== 1) {
        return undefined;
    }

    for (let i = 0; i < sentence.length; i++) {
        if (sentence[i] === letter) {
            result++;
        }
    }

    return result;
}

function isIsogram(text) {
    text = text.toLowerCase();
    for (let i = 0; i < text.length; i++) {
        for (let j = i + 1; j < text.length; j++) {
            if (text[i] === text[j]) {
                return false;
            }
        }
    }
    return true;
}

function purchase(age, price) {
    if (age < 13) {
        return undefined;
    } else if (age >= 13 && age <= 21 || age >= 65) {
        return Math.round(price * 0.8).toString();
    } else {
        return Math.round(price).toString();
    }
}

function findHotCategories(items) {
    let hotCategories = [];
    let categories = new Set();

    for (let i = 0; i < items.length; i++) {
        if (items[i].stocks === 0 && !categories.has(items[i].category)) {
            hotCategories.push(items[i].category);
            categories.add(items[i].category);
        }
    }

    return hotCategories;
}

function findFlyingVoters(candidateA, candidateB) {
    let setA = new Set(candidateA);
    let setB = new Set(candidateB);
    let commonVoters = [];

    for (let voter of setA) {
        if (setB.has(voter)) {
            commonVoters.push(voter);
        }
    }

    return commonVoters;
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};
